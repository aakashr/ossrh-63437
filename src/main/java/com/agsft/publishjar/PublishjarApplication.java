package com.agsft.publishjar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublishjarApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublishjarApplication.class, args);
	}

}
